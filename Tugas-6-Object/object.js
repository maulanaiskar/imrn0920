//soal 1. array to object
function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear()

    for (var i = 0; i < arr.length; i++) {
        var obj = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: thisYear - arr[i][3]
        }
        if (!arr[i][3] || arr[i][3] > thisYear) {
            obj.age = "Invalid Birth Year"
        }
        console.log(i + 1 + ". " + obj.firstName + " " + obj.lastName + ":")
        console.log(obj)
    }
    return "";
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)

arrayToObject([])

console.log("================ soal 2 ================");
//soal no. 2 shopping time
function shoppingTime(memberID, money) {
    if (!memberID) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    } else {
        var i = 0;
        var buy = {
            memberID: memberID,
            money: money,
            listPurchased: [],
            changeMoney: money
        }
        if (money >= 1500000) {
            buy.listPurchased[i] = "Sepatu Stacattu"
            money -= 1500000
            i++
        } if (money >= 500000) {
            buy.listPurchased[i] = "Baju Zoro"
            money -= 500000
            i++
        } if (money >= 250000) {
            buy.listPurchased[i] = "Baju H&N"
            money -= 250000
            i++
        } if (money >= 175000) {
            buy.listPurchased[i] = "Sweater Unikloo"
            money -= 175000
            i++
        } if (money >= 50000) {
            buy.listPurchased[i] = "Casing Handphone"
            money -= 50000
            i++
        }
        buy.changeMoney = money
        return buy
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

console.log("================ soal 3 ================");
function naikAngkot(listPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F']
    var ongkos = 0
    var arr = []
    for (var i = 0; i < listPenumpang.length; i++) {
        var isiAngkot = {
            penumpang: listPenumpang[i][0],
            naikDari: listPenumpang[i][1],
            tujuan: listPenumpang[i][2],
            bayar: ongkos
        }

        var asal = rute.indexOf(isiAngkot.naikDari)
        var tujuan = rute.indexOf(isiAngkot.tujuan)
        ongkos = tujuan - asal
        isiAngkot.bayar = ongkos * 2000
        arr[i] = isiAngkot
    }
    return arr
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));