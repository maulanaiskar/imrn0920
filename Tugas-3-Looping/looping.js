//soal 1. looping while
var i = 0
console.log("LOOPING PERTAMA");
while (i <= 18) {
    i += 2;
    console.log(i + " - I love coding");
}

console.log("LOOPING KEDUA");
while (i >= 2) {
    console.log(i + " - I will become a mobile developer");
    i -= 2;
}

console.log();
//soal 2. looping for
for (var index = 1; index <= 20; index++) {
    if (index % 2 == 1) {
        if (index % 3 == 0) {
            console.log(index + " - I love Coding");
        } else {
            console.log(index + " - Santai");
        }
    } else if (index % 2 == 0) {
        console.log(index + " - Berkualitas");
    }
}

console.log();
//soal 3. membuat persegi panjang
var karakter = "";
for (var i = 0; i < 4; i++) {
    for (var j = 0; j < 8; j++) {
        karakter += "#";
    }
    console.log(karakter);
    karakter = "";
}

console.log();
//soal 4. membuat segitiga
var karakter2 = "";
for (var k = 0; k < 1; k++) {
    for (var l = 0; l < 7; l++) {
        karakter2 += "#";
        console.log(karakter2);
    }
}

console.log();
//soal 5. membuat papan catur
karakter3 = "";
for (var m = 0; m < 8; m++) {
    for (var n = 0; n < 8; n++) {
        if (n % 2 == 1) {
            if (m % 2 == 0) {
                karakter3 += "#";
            } else {
                karakter3 += " ";
            }
        } else if (n % 2 == 0) {
            if (m % 2 == 0) {
                karakter3 += " ";
            } else {
                karakter3 += "#";
            }
        }
    }
    console.log(karakter3);
    karakter3 = "";
}