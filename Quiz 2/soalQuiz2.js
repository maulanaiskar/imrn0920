class Score {
    constructor(subject, points, email) {
        this.subject = subject,
            this._points = points,
            this.email = email
    }

    get points() {
        return this._points;
    }

    set points(x) {
        return this._points = x;
    }

    average(...nilai) {
        let totalNilai = 0;
        for (let temp of nilai) {
            totalNilai = totalNilai + temp;
        }
        let rata = totalNilai / nilai.length
        return rata
    }
}
nilaiQuiz = new Score()
console.log(nilaiQuiz.average(45, 90, 85))
console.log("\n")

//soal 2
const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

var viewScores = (data, subject) => {
    var subjek = subject
    var arrayJawaban = []
    if (subject == "quiz-1") {
        var nilaiKe = 1
    } else if (subject == "quiz-2") {
        var nilaiKe = 2
    } else { var nilaiKe = 3 }
    for (var i = 1; i < 5; i++) {
        var object = {
            email: data[i][0],
            subject: subjek,
            points: data[i][nilaiKe],
        }
        arrayJawaban[i - 1] = object
    }
    console.log(arrayJawaban)
    return ''
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")
console.log("\n")

//soal 3
var recapScores = (data) => {
    for (var i = 1; i < 5; i++) {
        rata2 = (data[i][1] + data[i][2] + data[i][3]) / 3
        if (rata2 > 70 && rata2 < 81) { var predikat = "participant" }
        else if (rata2 > 80 && rata2 < 91) { var predikat = "graduate" }
        else if (rata2 > 90) { var predikat = "honour" }
        console.log(`${i}. Email: ${data[i][0]} 
      Rata-rata: ${rata2}
      Predikat: ${predikat}`)
    }
}

recapScores(data);
