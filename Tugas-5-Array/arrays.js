// soal 1. range
console.log("==================soal 1==================");
function range(startNum, finishNum) {
    var himpunan = []
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            himpunan.push(i)
        }
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i--) {
            himpunan.push(i)
        }
    } else if (typeof startNum === "undefined" || typeof finishNum === "undefined") {
        return -1;
    }
    return himpunan;
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

console.log("==================soal 2==================");
//soal 2. range with step
function rangeWithStep(startNum, finishNum, step) {
    var himpunan = []
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i += step) {
            himpunan.push(i)
        }
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i -= step) {
            himpunan.push(i)
        }
    }
    return himpunan;
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

console.log("==================soal 3==================");
//soal 3. sum
function sum(awalDeret, akhirDeret, step = 1) {
    var total = 0;
    var himpunan = []
    if (awalDeret < akhirDeret) {
        for (var i = awalDeret; i <= akhirDeret; i += step) {
            himpunan.push(i)
        }
    } else if (awalDeret > akhirDeret) {
        for (var i = awalDeret; i >= akhirDeret; i -= step) {
            himpunan.push(i)
        }
    } else if (!awalDeret) {
        return 0;
    } else if (!akhirDeret) {
        return 1;
    }

    for (var i = 0; i < himpunan.length; i++) {
        total += himpunan[i];
    }

    return total;
}

console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

console.log("==================soal 4==================");
//soal 4. array multi

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function dataHandling(input) {

    for (var i = 0; i < input.length; i++) {
        console.log("Nomor ID: " + input[i][0])
        console.log("Nama Lengkap: " + input[i][1])
        console.log("TTL: " + input[i][2])
        console.log("Hobi: " + input[i][3])
        console.log();
    }
}

dataHandling(input)

console.log("==================soal 5==================");
//soal 5. balik kata
function balikKata(kata) {
    var kataBaru = '';
    for (var i = kata.length - 1; i >= 0; i--)
        kataBaru += kata[i];
    return kataBaru;
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

console.log("==================soal 6==================");
//soal 6. metode array
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

function dataHandling2(input) {
    input.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
    console.log(input)
    var bulanUntukDesc = input[3].split("/")
    var bulanUntukStrip = input[3].split("/")
    var bulanAja = bulanUntukStrip[1]
    switch (bulanAja) {
        case '01':
            console.log("Januari");
            break;
        case '02':
            console.log("Februari");
            break;
        case '03':
            console.log("Maret");
            break;
        case '04':
            console.log("April");
            break;
        case '05':
            console.log("Mei");
            break;
        case '06':
            console.log("Juni");
            break;
        case '07':
            console.log("Juli");
            break;
        case '08':
            console.log("Agustus");
            break;
        case '09':
            console.log("September");
            break;
        case '10':
            console.log("Oktober");
            break;
        case '11':
            console.log("Nopember");
            break;
        case '12':
            console.log("Desember");
            break;
        default:
            console.log("Maaf bulan yang anda masukkan salah");
            break;
    }
    //descending
    var bulanDesc = bulanUntukDesc.sort(function (a, b) {
        return b - a
    })
    console.log(bulanDesc);

    //join dg -
    var bulanBaru = bulanUntukStrip.join("-")
    console.log(bulanBaru);

    //nama
    var namaBaru = input[1].slice(0, 14)
    console.log(namaBaru);
}

dataHandling2(input)